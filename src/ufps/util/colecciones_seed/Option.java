/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ufps.util.colecciones_seed;

/**
 *
 * @author Sachica
 */
public class Option<T> {
    ListaS<T> x;
    Nodo<T> y;

    public Option() {
        this.x = new ListaS<>();
    }

    public Option(ListaS<T> x, Nodo<T> y) { //----->2
        this.x = x;                         //-->1
        this.y = y;                         //-->1
    }

    public String toString() {
        return this.x.toString() + "  -  " + ((this.y != null) ? this.y.getInfo().toString() : "null");
    }
}
